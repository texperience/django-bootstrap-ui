# Templates

django-bootstrap-ui enables you to quickly prepare your page for Bootstrap and provide your content through a set of predefined templates.

## HTML5 skeleton

This is our root template (`html5-skeleton.html`) with a minimalistic and clean HTML5 skeleton. It comes with a well-thought-out block structure as follows, but is not meant to be extended directly, at least not if you want to use Bootstrap.

### head-meta

Provide additional meta tags within the `<head>`. Example:
    
```django
{% block head-meta %}
    <meta name="keywords" content="wood, furniture, garden, garden-table, etc.">
{% endblock %}
```

### head-title

Provide your content for `<title>`. Example:

```django
{% block head-title %}
    Your title
{% endblock %}
```

### head-css

Provide additional `<link>` tags with css references. Example:

```django
{% block head-css %}
    <link href="css/your-theme.css" media="screen" rel="stylesheet" type="text/css">
{% endblock %}
```

### head-javascript

Provide additional `<script>` tags with javascript source references or inline code. Example:

```django
{% block head-javascript %}
    <script src="js/your-script.js" type="text/javascript"></script>
{% endblock %}
```

### head-extension

Provide other content within the `<head>`. Example:

```django
{% block head-extension %}
    Whatever content you need at the end of the head.
{% endblock %}
```

### body-content

All your page content in `<body>`. Example:

```django
{% block body-content %}
    <h1>Hello, django-bootstrap-ui!</h1>
{% endblock %}
```

### body-javascript

Provide additional `<script>` tags with javascript source references or inline code just before `<body>` ends. Example:

```django
{% block body-javascript %}
    <script src="js/your-lately-embedded-script.js" type="text/javascript"></script>
{% endblock %}
```

## Bootstrap skeletons

The templates `bootstrap5-skeleton.html`, `bootstrap4-skeleton.html` and `bootstrap3-skeleton.html` extend our `html5-skeleton.html` and provide Bootstrap support. Extend them to get a working, bootstraped page layout.

```django
{% extends "bootstrap_ui/bootstrap5-skeleton.html" %}
```

```django
{% extends "bootstrap_ui/bootstrap4-skeleton.html" %}
```

```django
{% extends "bootstrap_ui/bootstrap3-skeleton.html" %}
```

There are no additional blocks besides the above, the following ones are sensibly used by this template:

* `head-meta`
* `head-css`
* `head-javascript`
* `body-javascript`

Be careful when you are going to use these blocks with your own stuff. Remember to apply `{{ block.super }}` so you won't overwrite existing and necessary Bootstrap resources. Example:

```django
{% block head-css %}
    {{ block.super }}

    <link href="css/your-theme.css" media="screen" rel="stylesheet" type="text/css">
{% endblock %}
```
