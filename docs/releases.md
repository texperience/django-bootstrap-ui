# Release notes

## [3.1.0-dev]

## [3.0.0]
### Added
* Provide full-featured Bootstrap 5 base template

### Changed
* Upgrade Bootstrap 3 template to 3.4.1
* Upgrade Bootstrap 4 template to 4.6.2
* Write docs in markdown using mkdocs as generator
* Deploy docs to GitLab pages instead of readthedocs

### Removed
* Remove old browser support through html5shiv
* Purge builtin theming support

## [2.0.0]
### Added
* Add support for Python 3.9 and 3.10
* Add support for Django 3.0, 3.1, 3.2 and 4.0

### Changed
* Upgrade Bootstrap 4 template to 4.6.1

### Removed
* Remove support for Python 3.6

## [1.1.0]
### Added
* Provide full-featured Bootstrap 4 base template
* Integrate Google material design icon font in Bootstrap 4 template

## [1.0.6]
### Fixed
* Fixed docs version

## [1.0.5]
### Changed
* Improve documentation configuration

## [1.0.4]
### Fixed
* Specify master_doc for sphinx on rtd

## [1.0.3]
### Changed
* Simplify deploy command

### Removed
* Remove auto-documentation of tag reference

## [1.0.2]
### Fixed
* Include necessary package data

## [1.0.1]
### Fixed
* Find namespaced packages

## [1.0.0]
### Added
* Add support for Python 3.7 and 3.8
* Add support for Django 2.2
* Initialise continuous integration with gitlab
* Build distribution after success in test stage
* Deploy build artifacts on tagged commits to pypi

### Changed
* Rename bootstrap 3 template for clarification
* Move project to gitlab

### Removed
* Remove support for Python 2.7, 3.3, 3.4 and 3.5
* Remove support for Django 1.8, 1.9, 1.10 and 1.11
* Remove bootstrap template tag api
* Remove bootstrap component templates

## [0.5.1]
### Changed
* Re-packaged for PyPI deployment

## [0.5.0]
### Added
* Added support for Django 1.11
* Added support for Python 3.6

## [0.4.0]
### Added
* Added support for Django 1.10

### Changed
* Upgraded to Bootstrap 3.3.7
* Upgraded to jQuery 1.12.4
* Upgraded to Font Awesome 4.7.0

### Removed
* Dropped support for Django 1.7

## [0.3.0]
### Changed
* Upgraded to Bootstrap 3.3.6
* Upgraded to Font Awesome 4.5.0
* Moved project to texperience

## [0.2.0]
### Added
* Implemented grid column offsets
* Added grid column pushes and pulls
* Added support for Django 1.9
* Added support for Python 3.5

## [0.1.0]
### Added
* Implemented session-based theme switcher
* Integrated Bootstrap and Bootswatch themes
* Provided template for list groups
* Implemented panels in template tag API
* Implemented list groups in template tag API
* Implemented grid system in template tag API
* Introduced template tag API
* Provided full-featured Bootstrap base template
* Provided clean HTML5 base template
* Integrated Font Awesome 4.3.0
* Implemented codebase on Bootstrap 3.3.5
* Established support for Django 1.7 and 1.8
* Established support for Python 2.7, 3.3 and 3.4
