# Code and contribution

The code is open source and released under the [ISC License (ISCL)][isc-license]. It is available on [Gitlab][gitlab] and follows the guidelines about [Semantic Versioning][semver] for transparency within the release cycle and backward compatibility whenever possible.

All contributions are welcome, whether bug reports, code contributions and reviews, documentation or feature requests.

## Development
If you're a developer, fork the repo and prepare a merge request:

```bash title="Prepare your environment the first time"
python3.10 venv .venv
source .venv/bin/activate
pip install -e .[development]
```

```bash title="Running the tests while development"
python runtests.py
```

```bash title="Individual supported versions tests and code quality checks"
tox -e py310-dj32
tox -e flake8
```

```bash title="Ensure code quality running the entire test suite"
tox
```

[isc-license]: http://en.wikipedia.org/wiki/ISC_license
[semver]: http://semver.org/
[gitlab]: https://gitlab.com/texperience/django-bootstrap-ui

## Documentation

All documentation is written in [Markdown][markdown] and uses [MkDocs][mkdocs] as a static site generator.

```bash title="Start docs development server"
mkdocs serve
```

On every new Git tag the docs are automatically built in the CI pipeline and deployed using [GitLab Pages][gitlab-pages] under [https://texperience.gitlab.io/django-bootstrap-ui][django-bootstrap-ui-docs].

[markdown]: https://daringfireball.net/projects/markdown/
[mkdocs]: https://www.mkdocs.org/
[gitlab-pages]: https://docs.gitlab.com/ee/user/project/pages/
[django-bootstrap-ui-docs]: https://texperience.gitlab.io/django-bootstrap-ui
