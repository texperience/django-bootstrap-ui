# Getting started

## Installation

First install the package using `pip`:

```bash
pip install django-bootstrap-ui
```

Then add `bootstrap_ui` to your `INSTALLED_APPS` setting:

```python
INSTALLED_APPS = (
    ...
    'bootstrap_ui',
    ...
)
```

## Usage

Prepare your page for HTML5, Bootstrap 3, 4 or 5 using our base templates and provide your content. Extend `html5-skeleton.html`, `bootstrap3-skeleton.html`, `bootstrap4-skeleton.html` or `bootstrap5-skeleton.html` in your base template:

```django
{% extends "bootstrap_ui/bootstrap5-skeleton.html" %}
```

Below fill predefined blocks with your content:

```django
{% block body-content %}
    <h1>Hello, I'm using django-bootstrap-ui!</h1>
{% endblock %}
```
