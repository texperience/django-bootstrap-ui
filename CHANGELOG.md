# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog][keepachangelog], and this project adheres to [Semantic Versioning][semver].

## [3.1.0-dev] - Unreleased

## [3.0.0] - 2023-10-16
### Added
* Provide full-featured Bootstrap 5 base template

### Changed
* Upgrade Bootstrap 3 template to 3.4.1
* Upgrade Bootstrap 4 template to 4.6.2
* Write docs in markdown using mkdocs as generator
* Deploy docs to GitLab pages instead of readthedocs

### Removed
* Remove old browser support through html5shiv
* Purge builtin theming support

## [2.0.0] - 2021-12-20
### Added
* Add support for Python 3.9 and 3.10
* Add support for Django 3.0, 3.1, 3.2 and 4.0

### Changed
* Improve pep8 compliance
* Simplify package information and metadata
* Upgrade Bootstrap 4 template to 4.6.1
* Simplify release documentation structure

### Removed
* Remove support for Python 3.6

## [1.1.0] - 2020-02-13
### Added
* Provide full-featured Bootstrap 4 base template
* Integrate Google material design icon font in Bootstrap 4 template

### Changed
* Split readme with directive for use in docs without duplication
* Simplify release notes documentation
* Improve test coverage for set_theme

## [1.0.6] - 2020-02-09
### Fixed
* Fixed docs version

## [1.0.5] - 2020-02-09
### Changed
* Improve documentation configuration

## [1.0.4] - 2020-02-09
### Fixed
* Specify master_doc for sphinx on rtd

## [1.0.3] - 2020-02-09
### Changed
* Simplify deploy command

### Removed
* Remove auto-documentation of tag reference

## [1.0.2] - 2020-02-09
### Fixed
* Include necessary package data

## [1.0.1] - 2020-02-09
### Fixed
* Find namespaced packages

## [1.0.0] - 2020-02-08
### Added
* Add support for Python 3.7 and 3.8
* Add support for Django 2.2
* Initialise continuous integration with gitlab
* Build distribution after success in test stage
* Deploy build artifacts on tagged commits to pypi

### Changed
* Rename bootstrap 3 template for clarification
* Move project to gitlab

### Removed
* Remove support for Python 2.7, 3.3, 3.4 and 3.5
* Remove support for Django 1.8, 1.9, 1.10 and 1.11
* Remove bootstrap template tag api
* Remove bootstrap component templates

## [0.5.1] - 2017-10-01
### Changed
* Re-packaged for PyPI deployment

## [0.5.0] - 2017-10-01
### Added
* Added support for Django 1.11
* Added support for Python 3.6

## [0.4.0] - 2016-11-19
### Added
* Added support for Django 1.10

### Changed
* Upgraded to Bootstrap 3.3.7
* Upgraded to jQuery 1.12.4
* Upgraded to Font Awesome 4.7.0

### Removed
* Dropped support for Django 1.7

## [0.3.0] - 2016-11-05
### Changed
* Upgraded to Bootstrap 3.3.6
* Upgraded to Font Awesome 4.5.0
* Moved project to texperience

## [0.2.0] - 2015-12-13
### Added
* Implemented grid column offsets
* Added grid column pushes and pulls
* Added support for Django 1.9
* Added support for Python 3.5

## [0.1.0] - 2015-08-03
### Added
* Implemented session-based theme switcher
* Integrated Bootstrap and Bootswatch themes
* Provided template for list groups
* Implemented panels in template tag API
* Implemented list groups in template tag API
* Implemented grid system in template tag API
* Introduced template tag API
* Provided full-featured Bootstrap base template
* Provided clean HTML5 base template
* Integrated Font Awesome 4.3.0
* Implemented codebase on Bootstrap 3.3.5
* Established support for Django 1.7 and 1.8
* Established support for Python 2.7, 3.3 and 3.4

[keepachangelog]: https://keepachangelog.com/en/1.0.0/
[semver]: https://semver.org/spec/v2.0.0.html
