# Welcome to django-bootstrap-ui

[![Pipeline][pipeline-badge]][pipeline-link]
[![Docs][docs-badge]][docs-link]
[![PyPI][pypi-badge]][pypi-link]
[![License][license-badge]][license-link]

[pipeline-badge]: https://gitlab.com/texperience/django-bootstrap-ui/badges/master/pipeline.svg?ignore_skipped=true
[pipeline-link]: https://gitlab.com/texperience/django-bootstrap-ui/pipelines
[pypi-badge]: https://img.shields.io/pypi/v/django-bootstrap-ui.svg
[pypi-link]: https://pypi.python.org/pypi/django-bootstrap-ui
[license-badge]: https://img.shields.io/pypi/l/django-bootstrap-ui.svg
[license-link]: http://en.wikipedia.org/wiki/ISC_license
[docs-badge]: https://img.shields.io/badge/docs-stable-brightgreen.svg
[docs-link]: https://django-bootstrap-ui.texperience.de

django-bootstrap-ui simplifies the integration of the popular [Bootstrap UI framework][bootstrap]. It is written in [Python][python] and built on the [Django web framework][django]. Give it a try and head over to the [documentation][docs-link]!

[bootstrap]: http://getbootstrap.com/
[django]: https://www.djangoproject.com/
[python]: https://www.python.org/

## Basic features

* Basic HTML5 template
* Extensive and up-to-date documentation
* Mainstream Python and Django support

## Bootstrap 5 features

* Full-featured Bootstrap 5 template (5.3.2)
* Integrated Google material design icon font

## Bootstrap 4 features

* Full-featured Bootstrap 4 template (4.6.2)
* Integrated Google material design icon font

## Bootstrap 3 features

* Full-featured Bootstrap 3 template (3.4.1)
* Latest Font Awesome integration (4.7.0)

## Technical requirements

New feature releases frequently add support for newer versions of underlying technologies and drop support for older ones. The compatible versions of Django and Python currently are:

| Django   | Python               |
|----------|----------------------|
| 4.0      | 3.8, 3.9, 3.10       |
| 3.2 LTS  | 3.7, 3.8, 3.9, 3.10  |
| 3.1      | 3.7, 3.8, 3.9        |
| 3.0      | 3.7, 3.8, 3.9        |
| 2.2 LTS  | 3.7, 3.8, 3.9        |
