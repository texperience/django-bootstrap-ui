from django.template.loader import get_template
from django.test import RequestFactory, TestCase


class TemplatesTest(TestCase):
    def setUp(self):
        # Mock get request
        self.request = RequestFactory().get('/')

        # Provide template paths
        self.template_html5_skeleton = get_template('bootstrap_ui/html5-skeleton.html')
        self.template_bootstrap3_skeleton = get_template('bootstrap_ui/bootstrap3-skeleton.html')
        self.template_bootstrap4_skeleton = get_template('bootstrap_ui/bootstrap4-skeleton.html')
        self.template_bootstrap5_skeleton = get_template('bootstrap_ui/bootstrap5-skeleton.html')

    def test_html5_skeleton_is_rendered(self):
        rendered = self.template_html5_skeleton.render({})
        self.assertInHTML(
            '<html lang="en"><head><meta charset="utf-8"><title>django-bootstrap-ui template</title></head>'
            '<body><h1>Hello, django-bootstrap-ui!</h1></body></html>', rendered)

    def test_bootstrap3_skeleton_is_rendered(self):
        rendered = self.template_bootstrap3_skeleton.render({'request': self.request})
        self.assertInHTML('<meta http-equiv="X-UA-Compatible" content="IE=edge">', rendered)
        self.assertInHTML('<meta name="viewport" content="width=device-width, initial-scale=1">', rendered)
        self.assertInHTML('<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet" type="text/css">', rendered)
        self.assertInHTML('<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">', rendered)
        self.assertInHTML('<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>', rendered)
        self.assertInHTML('<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>', rendered)

    def test_bootstrap4_skeleton_is_rendered(self):
        rendered = self.template_bootstrap4_skeleton.render({'request': self.request})
        self.assertInHTML('<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">', rendered)
        self.assertInHTML('<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha256-+IZRbz1B6ee9mUx/ejmonK+ulIP5A5bLDd6v6NHqXnI=" crossorigin="anonymous">', rendered)
        self.assertInHTML('<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">', rendered)
        self.assertInHTML('<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js" integrity="sha256-QjIXq/h3XOotww+h/j4cXiTcNZqA8cN60pqGCUv+gdE=" crossorigin="anonymous"></script>', rendered)
        self.assertInHTML('<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>', rendered)
        self.assertInHTML('<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.1/umd/popper.min.js" integrity="sha256-/ijcOLwFf26xEYAjW75FizKVo5tnTYiQddPZoLUHHZ8=" crossorigin="anonymous"></script>', rendered)

    def test_bootstrap5_skeleton_is_rendered(self):
        rendered = self.template_bootstrap5_skeleton.render({'request': self.request})
        self.assertInHTML('<meta name="viewport" content="width=device-width, initial-scale=1">', rendered)
        self.assertInHTML('<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" integrity="sha256-MBffSnbbXwHCuZtgPYiwMQbfE7z+GOZ7fBPCNB06Z98=" crossorigin="anonymous">', rendered)
        self.assertInHTML('<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">', rendered)
        self.assertInHTML('<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.min.js" integrity="sha256-YMa+wAM6QkVyz999odX7lPRxkoYAan8suedu4k2Zur8=" crossorigin="anonymous"></script>', rendered)
