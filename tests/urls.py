from django.shortcuts import render
from django.urls import path

urlpatterns = [
    path('html5', render, {'template_name': 'bootstrap_ui/html5-skeleton.html'}),
    path('bootstrap3', render, {'template_name': 'bootstrap_ui/bootstrap3-skeleton.html'}),
    path('bootstrap4', render, {'template_name': 'bootstrap_ui/bootstrap4-skeleton.html'}),
    path('bootstrap5', render, {'template_name': 'bootstrap_ui/bootstrap5-skeleton.html'}),
]
