from setuptools import find_namespace_packages, setup

# Installation dependencies
install_requires = [
    'django>=2.2,<4.1',
]

# Development dependencies
building_extras = [
    'mkdocs-material>=9.4,<10.0',
]
linting_extras = [
    'flake8>=6.0,<7.0',
]
testing_extras = [
    'tox>=3.28,<4.0',
]
development_extras = building_extras + linting_extras + testing_extras

setup(
    packages=find_namespace_packages(exclude=['tests', 'docs']),
    include_package_data=True,
    install_requires=install_requires,
    extras_require={
        'development': development_extras,
        'building': building_extras,
        'linting': linting_extras,
        'testing': testing_extras,
    },
)
